
####################
# Tulsky V.A. 2024 #
####################
import os
import requests
import docker
from apscheduler.schedulers.background import BackgroundScheduler
import time
import logging
from packaging.version import Version
# See docs: https://github.com/python/cpython/blob/3.10/Lib/distutils/version.py

logging.basicConfig(level=logging.INFO)

known_versions_url = os.getenv("known_versions_url")
domain             = os.getenv("domain")
alerta_api_url     = os.getenv("alerta_api_url")
alerta_api_key     = os.getenv("alerta_api_key")
alerta_environment = os.getenv("alerta_environment")
alerta_service     = os.getenv("alerta_service")
alerta_service     = alerta_service.split(",")
alerta_event       = "VersionCheck"

logging.info(f" known_versions_url: {known_versions_url}")

client = docker.from_env()

def send_alert( alerta_resource: str,
                alerta_severity: str,
                alerta_service: str,
                alerta_event: str,
                alerta_description: str,
                alerta_value: str = "") -> None:
    
        if alerta_severity not in ["ok", "normal", "cleared"]:
            alerta_status = "open"
        else:
            alerta_status = "closed"

        json_payload = {
            "environment": alerta_environment,
            "severity":    alerta_severity,
            "status":      alerta_status,
            "event":       alerta_event,
            "service":     alerta_service,
            "resource":    alerta_resource,
            "text":        alerta_description,
            "value":       alerta_value
            }
        headers = {
            "Authorization": f"Key {alerta_api_key}",
            "Content-type": "application/json"
            }
        
        try:
            # response: some useful methods are ".json()", ".text()", ".status_code
            response = requests.post(alerta_api_url, headers=headers,
                                     json=json_payload, timeout=5)
            
        except requests.ConnectionError as e:
            logging.error(f" {e}")
            return
        
        if response.status_code != 201:
            logging.warning(f"alerta_api_url: {alerta_api_url}")
            logging.warning(f"alerta_api_key: {alerta_api_key}")
            logging.warning(f"Alert request response.status_code: {response.status_code}")
            logging.warning(f"Alert request response.content: {response.content}")


def check_versions():

    logging.info(f" Starting version check")

    containers = client.containers.list(all=True)

    try:
        response = requests.get(known_versions_url, timeout=5)
        known_images = response.json()["known_images"]        
    except Exception as e:
        alerta_description = f" Cannot get the known versions list from url: {known_versions_url}"
        alerta_severity = "major"
        logging.error(f" Error: {e}")
        return

    for i in known_images:
            if i["data_format"] == "gitlab":
                i["image"] = f'registry.gitlab.com/{i["image"]}'
    logging.debug(f" known_images: {known_images}")


    for container in containers:

        name = container.name

        logging.info(f" container: {name}")
        # logging.info(f" container.image.tags: {container.image.tags}")

        try:
            tag  = container.image.tags[0]
            logging.info(f" container.image: {container.image}")
        except:
            message = f"\n\n Cannot get tag for container {name}. Got container: {container}\n\n"
            logging.error(message)
            send_alert( f"{name} at {domain}", 
                        "critical", 
                        alerta_service,
                        alerta_event,
                        message,
                        alerta_value=version)
            logging.info(alerta_description)
            continue

        image   = tag.split(":")[0]
        version = tag.split(":")[1]

        for i in known_images:
            if i["image"] == image:

                latest_version = i["latest_version"]

                try:
                    oldest_version = i["oldest_version"]
                except:
                    oldest_version = latest_version

                alerta_description = f"Image {image} has version {version}"

                try:              
                    if Version(version) > Version(latest_version):
                        alerta_severity = "warning"
                        alerta_description += f" and is newer than known latest_version {latest_version}"

                    elif Version(version) < Version(oldest_version):
                        alerta_description += f" and is older than oldest_version {oldest_version}"
                        alerta_severity = "critical"

                    elif Version(version) < Version(latest_version):
                        alerta_description += f" and is older than latest_version {latest_version}"
                        alerta_severity = "minor"

                    else:
                        alerta_description += f" and is up to date. latest_version: {latest_version}"
                        alerta_severity = "ok"

                except:
                    alerta_description += f" and cannot be properly parced"
                    alerta_severity = "major"

                alerta_resource = f"{name} at {domain}"
                send_alert( alerta_resource, 
                            alerta_severity, 
                            alerta_service,
                            alerta_event,
                            alerta_description,
                            alerta_value=version)
                logging.info(alerta_description)

# Create a scheduler to run jobs every given interval
scheduler = BackgroundScheduler()
scheduler.add_job(func=check_versions, trigger="interval", seconds=60)
scheduler.start()

# Keep the script running
while True:
    try:
        time.sleep(30) # set some finite value to save resources
    except (KeyboardInterrupt, SystemExit):
        # Shut down the scheduler when exiting the script manually
        scheduler.shutdown()
        exit(1)
