# Documentation

This Ansible roles manages deployment and configuration of Netdata monitoring service. The documentation can be found here:

https://learn.netdata.cloud/docs/deployment-guides

# Alarms

When something behaves not as expected, the Netdata allows to track that and create alarms with severities 'warning' or 'critical'. 

https://learn.netdata.cloud/docs/alerts-&-notifications/

To define custom alarms, add or modify templated files in this role in `./templates/etc/netdata/health.d/filename.conf.j2`.

To get the default alarms settings, go to the Netdata Docker container with
`docker exec -it <netdata_container> bash`
and view the content of files in `cat /etc/netdata/orig/health.d/`.

# Alerts for Alerta

Netdata supports reporting the alarms for multiple other services including Alerta. Generating alerts for Alerta from the Netdata alarms is configured in `health_alarm_notify.conf.j2` and `netdata.conf.j2` templates in the template folder `./templates/etc/netdata/health.d` of the current Ansible role.

# Dashboard

Netdata provides a web application with dashboard for a grapgical review of the current status of all monitoring components. This contains sensible data and should not be exposed to public unless for development or testing purposes. To not expose the dashboard, set the Ansible default variable `netdata_with_traefik_route: false` in `./defaults/main.yaml`.

If you choose to expose the dashboard, this role will configure the Traefik reverse proxy to serve the dashboard at `https://{{netdata_service_name}}` as defined in `./vars/main.yaml`.

# Single-sign-on (SSO)

Configuring access to the dashboard via the private SSO is currently not possible out of the box. A possible solution could be building and additional layer with oauth2 proxy for Netdata dashboard, but this is not yet implemented in the current Ansible role.
